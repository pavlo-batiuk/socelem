from django.conf.urls import url
from rest_framework. authtoken.views import obtain_auth_token
from content import views

urlpatterns = [
    url(r'^post/$', views.PostView.as_view(), name='post'),
    url(r'^post/like/$', views.LikeView.as_view(), name='post-like'),
    url(r'^post/dislike/$', views.DislikeView.as_view(), name='post-dislike')
]