# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from content.models import Post

from django.contrib import admin

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'created')

admin.site.register(Post, PostAdmin)
