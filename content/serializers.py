from content.models import Post
from users.serializers import UserSerializer
from rest_framework import serializers


# class LikesSerializer(serializers.PrimaryKeyRelatedField):
#
#     def get_queryset(self):
#         user = self.context['request'].user
#         is_like


class PostSerializer(serializers.ModelSerializer):
    author = UserSerializer()
    owner = UserSerializer()
    likes = serializers.SerializerMethodField(source='get_likes')
    dislikes = serializers.SerializerMethodField(source='get_dislikes')

    class Meta:
        model = Post
        fields = ('id', 'content', 'created', 'author', 'owner', 'likes', 'dislikes')

    def get_likes(self, obj):
        user = self.context.get('user')
        if not user:
            user = self.context['request'].user
        if obj.likes:
            is_liked = obj.likes.filter(id=user.id).exists()
            likes_count = obj.likes.count()
            return {'likes_count': likes_count, 'user_liked': is_liked}
        return {'likes_count': 0, 'user_liked': False}

    def get_dislikes(self, obj):
        user = self.context.get('user')
        if not user:
            user = self.context['request'].user
        if obj.dislikes:
            is_liked = obj.dislikes.filter(id=user.id).exists()
            likes_count = obj.dislikes.count()
            return {'dislikes_count': likes_count, 'user_disliked': is_liked}
        return {'dislikes_count': 0, 'user_disliked': False}
