# -*- coding: utf-8 -*-
from django.db import models
from users.models import User
from base.constants import NON, LIKE, DISLIKE
import uuid
import datetime

LIKE_CHOICES = (
    (NON, "non"),
    (LIKE, "like"),
    (DISLIKE, "dislike"),
)


class Comment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, unique=True, editable=False)
    content = models.TextField(default='')
    created = models.DateTimeField(default=datetime.datetime.now)
    author = models.ForeignKey(User)

    class Meta:
        ordering = ['-created']


class Post(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, unique=True, editable=False)
    content = models.TextField(default='')
    created = models.DateTimeField(default=datetime.datetime.now)
    owner = models.ForeignKey(User, null=True, related_name='owner')
    author = models.ForeignKey(User, related_name='author')
    comments = models.ForeignKey(Comment, null=True)
    likes = models.ManyToManyField(User, related_name='user_like')
    dislikes = models.ManyToManyField(User, related_name='user_dislike')

    class Meta:
        ordering = ['-created']

# from mongoengine import *
# class Comment(EmbeddedDocument):
#     content = StringField()
#     author = UUIDField(binary=False, null=True)
#     created = DateTimeField(default=datetime.datetime.utcnow)
#
#
# class Post(Document):
#     id = UUIDField(binary=False, dafault=uuid.uuid4, unique=True, required=True)
#     title = StringField(required=True, max_length=200)
#     created = DateTimeField(default=datetime.datetime.utcnow)
#     author = UUIDField(binary=False, null=True)
#     comments = ListField(EmbeddedDocumentField(Comment))
#     likes = ListField(UUIDField(binary=False, null=True))
#     dislikes = ListField(UUIDField(binary=False, null=True))
#     meta = {'allow_inheritance': True}
#
# class Wall(Document):
#     owner = UUIDField(binary=False, null=True)
#     posts = ListField(EmbeddedDocumentField(Post))