# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, render_to_response
from rest_framework import views, status
from rest_framework.authtoken.models import Token
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.authentication import TokenAuthentication
from rest_framework import permissions
from users.models import User
from content.models import Post
from content.serializers import PostSerializer
from django.shortcuts import render
import json


class PostView(views.APIView):

    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def post(self, request, *args, **kwargs):
        try:
            if not request.data.get('userId'):
                return Response('Post id is required', status=status.HTTP_400_BAD_REQUEST)
            user = User.objects.get(id=request.data['userId'])
            post = Post(
                author=request.user,
                content=request.data.get('content')
            )
            post.save()
            user.owner.add(post)
            user_posts = Post.objects.filter(owner=user)
            user_posts_serializer = PostSerializer(user_posts, many=True, context={'request': request})
            return Response(user_posts_serializer.data, status=status.HTTP_200_OK)

        except (TypeError, KeyError, ObjectDoesNotExist, IntegrityError) as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            post_id = data.get('postId')
            user_id = data.get('userId')
            if not post_id:
                return Response('Post id is required', status=status.HTTP_400_BAD_REQUEST)
            Post.objects.filter(id=post_id).delete()
            user_posts = Post.objects.filter(owner__id=user_id)
            user_posts_serializer = PostSerializer(user_posts, many=True, context= {'request': request})
            return Response(user_posts_serializer.data, status=status.HTTP_200_OK)

        except (TypeError, KeyError, ObjectDoesNotExist, IntegrityError) as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)


class LikeView(views.APIView):

    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            if not data.get('postId'):
                return Response('Post id is required', status=status.HTTP_400_BAD_REQUEST)
            post = Post.objects.get(id=data.get('postId'), owner=data.get('userId'))
            if post.likes.filter(id=request.user.id).exists():
                post.likes.remove(request.user)
            else:
                post.likes.add(request.user)
                post.dislikes.remove(request.user)
            post_serializer = PostSerializer(post, context= {'request': request})
            return Response(post_serializer.data, status=status.HTTP_200_OK)

        except (TypeError, KeyError, ObjectDoesNotExist, IntegrityError) as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)


class DislikeView(views.APIView):

    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def post(self, request, *args, **kwargs):
        try:
            data = request.data
            if not data.get('postId'):
                return Response('Post id is required', status=status.HTTP_400_BAD_REQUEST)
            post = Post.objects.get(id=data.get('postId'), owner=data.get('userId'))
            if post.dislikes.filter(id=request.user.id).exists():
                post.dislikes.remove(request.user)
            else:
                post.dislikes.add(request.user)
                post.likes.remove(request.user)
            post_serializer = PostSerializer(post, context= {'request': request})
            return Response(post_serializer.data, status=status.HTTP_200_OK)

        except (TypeError, KeyError, ObjectDoesNotExist, IntegrityError) as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
