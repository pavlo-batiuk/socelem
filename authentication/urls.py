from django.conf.urls import url
from rest_framework. authtoken.views import obtain_auth_token
from authentication import views

urlpatterns = [
    url(r'^$', views.AuthenticationView.as_view(), name='authentication'),
    #url(r'^api-token-auth/', obtain_auth_token),
]
