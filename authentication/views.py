from django.shortcuts import render, render_to_response
from rest_framework import views, status
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate, login
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.authentication import TokenAuthentication
from django.contrib.sessions.backends.db import SessionStore
from rest_framework import permissions
from users.models import User
from users.serializers import UserSerializer
import json
# Create your views here.


def index(request):
    return render_to_response('index.html')


class AuthenticationView(views.APIView):

    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.AllowAny,)

    def post(self, request, **kwargs):
        user_serializer = UserSerializer(data=request.data)
        if user_serializer.is_valid():
            user = user_serializer.save()
            Token.objects.create(user=user)
            return Response(user_serializer.data, status=status.HTTP_201_CREATED)
        return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, **kwargs):
        user_serializer = UserSerializer(data=request.data)
        user = User.objects.get(email=user_serializer.initial_data['email'], password=user_serializer.initial_data['password'])
        token = Token.objects.filter(user=user).first()
        if not token:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        request.session['token'] = token.key
        return Response(json.dumps({'token': token.key, 'userId': str(user.id)}), status=status.HTTP_200_OK)


