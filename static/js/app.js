// Vue.component('todo-item', {
//   props: ['todo'],
//   template: '<li>{{ todo }}</li>'
// });
// var app = new Vue({
//   el: '#app',
//   data: {
//     message: 'Hello Vue.js!'
//   },
//   methods: {
//     reverseMessage: function () {
//       this.message = this.message.split('').reverse().join('')
//     }
//   }
// });

// import Login from './login-component.js';
// import Register from './register-component.js';
// 2. Определение путей
// Каждый путь должен указывать на компонент
// "Компонентом" может быть как созданный через Vue.extend()
// полноценный конструктор, так и просто объект с настройками компонента
// Вложенные пути будут рассмотрены далее.
const routes = [
  { path: '/user/:userId', component: MainPage },
  { path: '/find', component: FindPage },
  { path: '/friends', component: FriendsPage },
  { path: '/login', component: Login },
  { path: '/register', component: Register }
];

// 3. Создаём инстанс роутера с опцией `routes`
// Можно передать и другие опции, но пока не будем усложнять
const router = new VueRouter({
  routes // сокращение от routes: routes
});

// 4. Создаём и монтируем корневой инстанс Vue нашего приложения.
// Удостоверьтесь, что передали инстанс роутера в опции `router`,
// что позволит приложению знать о его наличии
const app = new Vue({
  router: router,
  data: function(){
    return {
      show_components: false
    }
  },
  created() {
    if (sessionStorage.token) {
      Vue.http.headers.common['Authorization'] = 'Token ' + sessionStorage.token;
      this.$http.get('user/').then(response => {
        Vue.account = response.body;
        console.log(Vue.account);
        this.show_components = true;
        if(!this.isPathExist(window.location.hash))
           window.location.href = '#/user/' + Vue.account.id;
      }, response => {
        console.log(response);
        // error callback
      })
    }
  },
  methods: {
    isPathExist: function (path) {
      for(var index = 0; index < routes.length; index++) {
        if(path === '#' + routes[index].path)
            return true;
      }
      return false;
    }
  }
}).$mount('#app');
