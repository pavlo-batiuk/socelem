const MainPage = {
    template:
    `<div class="col-xs-12 base-max-width">
        <div class="col-xs-3 profile-container">
            <profile v-if="is_show" v-bind:user="user"></profile>
        </div>
        <div class="col-xs-6">
            <div class="main-content-container">
                <user-wall v-if="is_show" v-bind:user="user"></user-wall>
            </div>
        </div>
        <div class="col-xs-3 navigate-container">
            <navigation v-if="is_show" v-bind:user="user"></navigation>
        </div>
    </div>`,
    components: {
        'profile': Profile,
        'navigation': Navigation,
        'user-wall': UserWall
    },
    data: function() {
        return {
            is_show: false,
            user: {}
        }
    },
    created() {
        this.$http.get('user/profile/'+ this.$route.params.userId + '/').then(response => {
            this.user = JSON.parse(response.bodyText);
            this.is_show = true;
        }, response => {
            console.log(response);
            window.location.href='#/login';
            // error callback
        });
    }
};