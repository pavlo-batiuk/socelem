const Profile = {
    template: `<div class="inner-profile-container">
            <div>
                <div class="text-align-center">
                    <div class="user-avatar">
                        <img class="user-image" v-bind:src="userAvatar">
                    </div>
                    <div class="main-text-color">
                        <h3>{{profile.first_name}} {{profile.last_name}}</h3>
                    </div>
                </div>
                <br>
                <hr>
                <div class="m-b-15 main-text-color uppercase information-title">
                    <span>Addition information:</span>
                </div>
                <div class="p-l-10">
                    <div v-if="profile.country">
                        <label class="information-field main-text-color">Country:</label>
                        <span class="f15">{{ profile.country }}</span>
                    </div>
                    <div v-if="profile.city">
                        <label class="information-field main-text-color">City:</label>
                        <span class="f15">{{ profile.city }}</span>
                    </div>
                    <div v-if="profile.street">
                        <label class="information-field main-text-color">Street:</label>
                        <span class="f15">{{ profile.street }}</span>
                    </div>
                    <div v-if="profile.birthday">
                        <label class="information-field main-text-color">Birthday:</label>
                        <span class="f15">{{ profile.birthday }}</span>
                    </div>
                </div>
                <hr>
            </div>
        </div>`,
    props: ['user'],
    data: function() {
        return {
            profile: {},
            userAvatar: ''
        }
    },
    created() {
        this.profile = this.user.profile;
        this.userAvatar = this.profile.avatar_url?this.profile.avatar_url: 'static/images/user-avatar.jpg';
    }
};
