const Navigation = {
    template: `<div class="navigation-links-container">
        <div class="navigate-header">
            <h3>Home page</h3>
        </div>
        <hr>
        <br>
        <div>
            <div class="navigate-link">
                <a href="javascript:void(0)" class="menu-link-color" v-on:click="getUserLink()">
                    - Home page
                </a>
            </div>
            <div class="navigate-link">
                <router-link to="/messages" class="menu-link-color">
                    - Messages
                </router-link>
            </div>
            <div class="navigate-link">
                <router-link to="/photos" class="menu-link-color">
                    - Photos
                </router-link>
            </div>
            <div class="navigate-link">
                <router-link to="/settings" class="menu-link-color">
                    - Settings
                </router-link>
            </div>
        </div>
    </div>`,
    data: function () {
        return {
            account: Vue.account
        }
    },
    methods: {
        getUserLink: function() {
            console.log(Vue.account);
            window.location.hash = '#/user/' + this.account.id;
        }
    }
};
