const UserWall = {
    template: `
    <div>
        <div class="post-container">
            <textarea class="col-xs-12 post-textarea" v-model="postText">
            </textarea>
            <a href="javascript:void(0)" class="col-xs-3 btn search-button f16" v-on:click="createPost()">
              <span>POST</span>
            </a>
        </div>
        <div class="user-wall-container">
            <ul class="padding-none">
                <li class="user-post" v-for="(post, index) in posts">
                    <div>
                        <div class="user-post-avatar">
                            <img class="user-image" v-bind:src="post.author.avatar_url || default_avatar">
                        </div>
                        <div>
                            <a href="javascript:void(0)" class="main-text-color underline f15">
                                <strong>{{post.author.first_name}} {{post.author.last_name}}</strong>
                            </a>
                            <a href="javascript:void(0)" class="remove-post" v-on:click="deletePost(post)">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div><p>{{post.content}}</p></div>
                    <hr>
                    <div>
                        <button type="button" class="btn btn-default btn-sm" v-on:click="likePost(post, index)">
                          <span v-bind:class="{ 'like-post-text': post.likes.user_liked }">
                              <span class="glyphicon glyphicon-thumbs-up"></span>
                              <span> Like ({{post.likes.likes_count}})</span>
                          </span>
                        </button>
                        <button type="button" class="btn btn-default btn-sm" v-on:click="unlikePost(post, index)">
                          <span v-bind:class="{ 'like-post-text': post.dislikes.user_disliked }">
                              <span class="glyphicon glyphicon-thumbs-up"></span>
                              <span> Unlike ({{post.dislikes.dislikes_count}})</span>
                          </span>
                        </button>
                        <div class="float-right m-t-5">{{post.created | formatDate}}</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>`,
    props: ['user'],
    data: function() {
        return {
            postText: '',
            posts: [],
            default_avatar: 'static/images/user-avatar.jpg'
        }
    },
    created: function () {
        this.posts = this.user.posts;
    },
    methods: {
        createPost: function() {
            if (!this.postText.length) {
                console.log('Empty content')
            } else {
                var postContent = {content: this.postText, userId: this.$route.params.userId};
                this.$http.post('wall/post/', postContent).then(response => {
                    this.posts = response.body;
                    this.postText = '';
                }, response => {
                    console.log(response);
                    // error callback
                });
            }
        },
        likePost: function(post, index) {
            var data = { userId: this.$route.params.userId, postId: post.id };
            this.$http.post('wall/post/like/', data).then(response => {
                Vue.set(this.posts, index, response.body);
            }, response => {
                console.log(response);
                // error callback
            });
        },
        unlikePost: function(post, index) {
            var data = { userId: this.$route.params.userId, postId: post.id };
            this.$http.post('wall/post/dislike/', data).then(response => {
                Vue.set(this.posts, index, response.body);
            }, response => {
                console.log(response);
                // error callback
            });
        },
        deletePost: function(post) {
            var data = { userId: this.$route.params.userId, postId: post.id };
            this.$http.delete('wall/post/', { body: data }).then(response => {
                this.posts = response.body;
            }, response => {
                console.log(response);
                // error callback
            });
        }
    }
};