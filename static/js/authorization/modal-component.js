const ModalWindow = Vue.component('modal', {
    template:
        `<div class="modal-mask" v-show="show" transition="modal">
            <div class="modal-wrapper">
              <div class="modal-container">
        
                <div class="modal-header">
                  {{ header }}
                </div>
                
                <div class="modal-body">
                  {{ body }}
                </div>
        
                <div class="modal-footer"
                  :class="{'border-none': !showcancel && !showok}">
                  <slot name="footer">
        
                    <button class="modal-default-button modal-button-ok"
                      :class="{'border-none': !showcancel==true}"
                      v-show="showok"
                      @click="okcallback()">
                      {{ oktext }}
                    </button>
        
                    <button class="modal-default-button modal-button-cancel"
                      v-show="showcancel"
                      @click="cancelcallback()">
                      {{ canceltext }}
                    </button>
        
                  </slot>
                </div>
              </div>
            </div>
        </div>`
});
