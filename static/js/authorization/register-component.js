const Register= {
    template:
    `<div class="text-align-center">
        <h1>Registration</h1>
        <br><hr><br>
        <form action="" method="post">
            <div>
                <div class="row-left">
                    <div class="m-b-15">
                        <input class="input-form" type="text" placeholder="Email" v-model="email">
                        <p v-if="errors.email" class="error-message">{{ errors.email }}</p>
                    </div>
                    <div class="m-b-15">
                        <input class="input-form" type="password" placeholder="Password" v-model="password">
                        <p v-if="errors.password" class="error-message">{{ errors.password }}</p>
                    </div>
                    <div class="m-b-15">
                        <input class="input-form" type="password" placeholder="Repeat password" v-on:blur="checkPasswords()" v-model="passwordRepeat">
                        <p v-if="errors.passwordRepeat" class="error-message">{{ errors.passwordRepeat }}</p>
                    </div>
                    <div class="m-b-15">
                        <input class="input-form" type="text" placeholder="Fist name" v-model="firstName">
                    </div>
                    <div class="m-b-15">
                        <input class="input-form" type="text" placeholder="Last name" v-model="lastName">
                    </div>
                </div>
                <div class="row-right">
                    <div class="m-b-15">
                        <input class="input-form" type="text" placeholder="Country" v-model="country">
                    </div>
                    <div class="m-b-15">
                        <input class="input-form" type="text" placeholder="State" v-model="state">
                    </div>
                    <div class="m-b-15">
                        <input class="input-form" type="text" placeholder="City" v-model="city">
                    </div>
                    <div class="m-b-15">
                        <input class="input-form" type="text" placeholder="Street" v-model="street">
                    </div>
                    <div class="m-b-15">
                        <input class="input-form" type="text" placeholder="Birthday" v-model="birthday">
                    </div>
                </div>
            </div>
            <div class="radio m-b-15">
              <input id="male" type="radio" value="true" v-model="is_male">
              <label class="radio-label" for="male">Male</label>
              <input id="female" type="radio" value="false" v-model="is_male">
              <label class="radio-label" for="female">Female</label>
            </div>
            <div>
                <a href="javascript:void(0)" class="btn search-button" v-on:click="registerUser()">
                  <span>Register</span>
                </a>
            </div>
        </form>    
    </div>`,

    data: function() {
        return {
            email: '',
            password: '',
            passwordRepeat: '',
            firstName: '',
            lastName: '',
            country: '',
            state: '',
            city: '',
            street: '',
            birthday: '',
            is_male: true,
            errors: {
                email: '',
                password: '',
                passwordRepeat: '',
                firstName: '',
                lastName: ''
            }
        }
    },
    methods: {
        registerUser: function () {
            if (!this.email) {
                this.errors.email = errorMessages.emailRequire;
            }
            else if (!this.privateMethods.__validateEmail(this.email)) {
                this.errors.email = errorMessages.emailNotValid;
            }
            else if (this.password.length < 6) {
                this.errors.password = errorMessages.passwordTooShort;
            } else if (this.errors.passwordRepeat) {
            } else {
                var registerForm = {
                    email: this.email,
                    password: this.password,
                    first_name: this.firstName,
                    last_name: this.lastName,
                    country: this.country,
                    state: this.state,
                    city: this.city,
                    street: this.street,
                    birthday: this.birthday,
                    is_male: this.is_male
                };
                Vue.http.headers.common['Authorization'] = '';
                this.$http.post('authentication/', registerForm).then(response => {
                    window.location.hash = '#/login';
                }, response => {
                    // error callback
                });
            }
        },
        checkPasswords: function() {
            if (this.password !== this.passwordRepeat) {
                this.errors.passwordRepeat = errorMessages.passwordDontMatch;
            } else {
                this.errors.passwordRepeat = '';
            }
        },
        // checkDateValid: function() {
        //     var now = new Date(this.birthday); // birthday = 'YYYY-mm-dd'
        //     console.log(now.format("yyyy-MM-dd"));
        //     // var formated_date = now.format("yyyy-mm-dd");
        //     // console.log(formated_date);
        // }
    },
    created: function () {
        this.privateMethods = {
            __validateEmail: function (email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
        }
    }
};
