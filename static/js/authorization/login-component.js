const Login = { 
    template: 
    `<div class="text-align-center" id="login-app">
        <form action="" method="post">
            <h1>Login</h1>
            <br><hr><br>
            <div class="m-b-15">
                <input class="input-form" type="text" placeholder="Email" v-model="email">
                <p v-if="errors.email" class="error-message">{{ errors.email }}</p>
            </div>
            <div class="m-b-15">
                <input class="input-form" type="password" placeholder="Password" v-model="password">
                <p v-if="errors.password" class="error-message">{{ errors.password }}</p>
            </div>
            <div>
                <a href="javascript:void(0)" class="btn search-button" v-on:click="loginUser()">
                  <span>Login</span>
                </a>
            </div>
        </form>      
    </div>`,
    data: function() {
        return {
            email: 'email@email.com',
            password: '111111',
            errors: {
                email: '',
                password: ''
            }
        }
    },
    methods: {
        loginUser: function () {
            if (!this.email) {
                this.errors.email = errorMessages.emailRequire;
            }
            else if (!this.privateMethods.__validateEmail(this.email)) {
                this.errors.email = errorMessages.emailNotValid;
            }
            else if (this.password.length < 6) {
                this.errors.password = errorMessages.passwordTooShort;
            } else {
                var loginForm = {
                    email: this.email,
                    password: this.password
                };
                this.$http.put('authentication/', loginForm).then(response => {
                    var jsonObject = JSON.parse(response.body);
                    sessionStorage.token = jsonObject.token;
                    Vue.http.headers.common['Authorization'] = 'Token ' + sessionStorage.token;
                    window.location.hash = '#/user/' + jsonObject.userId;
                }, response => {
                    // error callback
                });
            }
        }
    },
    created: function () {
        this.privateMethods = {
            __validateEmail: function (email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
        }
    }
};