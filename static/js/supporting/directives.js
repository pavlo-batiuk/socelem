const Dialog = Vue.extend({
  template: `
    <div v-if="show">
      Are you sure?
      <button @click="doYes">yes</button>
      <button @click="show = false">no</button>
    </div>
  `
});

Vue.directive('confirm', {
  bind(el, binding, vnode) {
    const yesMethod = binding.value;
    el.handleClick = (e) => {
      const data = { doYes: yesMethod, show: true };
      let dialog = new Dialog({ data: data }).$mount();
      document.getElementById('app').appendChild(dialog.$el);
    };
    el.addEventListener('click', el.handleClick);
  },
  unbind(el) {
    el.removeEventListener('click', el.handleClick);
  }
});
