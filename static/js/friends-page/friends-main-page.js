const FriendsPage = {
    template:
    `<div class="col-xs-12 base-max-width">
        <div class="col-xs-3 profile-container">
            <friends-navigation></friends-navigation>
        </div>
        <div class="col-xs-6">
            <div class="main-content-container">
                <friends-list></friends-list>
            </div>
        </div>
        <div class="col-xs-3 navigate-container">
            <navigation v-bind:user="user"></navigation>
        </div>
    </div>`,
    components: {
        'friends-list': FriendsList,
        'friends-navigation': FriendsNavigation,
        'navigation': Navigation
    },
    data: function() {
        return {
            is_show: false,
            user: {}
        }
    },
    created() {
        // this.$http.get('user/profile/'+ this.$route.params.userId + '/').then(response => {
        //     this.user = JSON.parse(response.bodyText);
        //     this.is_show = true;
        // }, response => {
        //     console.log(response);
        //     window.location.href='#/login';
        //     // error callback
        // });
    }
};

