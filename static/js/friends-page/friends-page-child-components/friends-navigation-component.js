const FriendsNavigation = {
    template: `<div class="inner-profile-container">
        <div>
            <div class="navigate-link">
                <a href="javascript:void(0)" class="menu-link-color">
                    My friends
                </a>
            </div>
            <div class="navigate-link">
                <router-link to="/messages" class="menu-link-color">
                    Requests
                </router-link>
            </div>
            <div class="navigate-link">
                <router-link to="/photos" class="menu-link-color">
                    Suggestions
                </router-link>
            </div>
        </div>
    </div>`,
    data: function () {
        return {
            account: Vue.account
        }
    }
};