const FriendsList = {
    template:
    `<div>
        <div class="search-input-container">
            <input type="text" class="col-xs-12 input-search m-b-15" placeholder="Find friends" v-model="searchText">
        </div>
        <div class="user-wall-container">
            <ul class="padding-none">
                <li class="user-search" v-for="(user, index) in users_list">
                    <div class="user-post-avatar">
                        <img class="user-image" v-bind:src="user.avatar_url || default_avatar">
                    </div>
                    <div class="p-t-5 p-b-5">
                        <a href="javascript:void(0)" class="main-text-color f18" v-on:click="goToUserPage(user)">
                            <strong>{{user.first_name}} {{user.last_name}}</strong>
                        </a>
                        <a v-if="!user.friend_request" href="javascript:void(0)" v-on:click="addUser(index, user)" class="btn search-button add-user f15">
                            <span class="glyphicon glyphicon-plus"></span> 
                            <span>Add to friends</span>
                        </a>
                        <div v-if="user.friend_request" href="javascript:void(0)" class="waiting-response add-user f15">
                            <span class="glyphicon glyphicon-time"></span> 
                            <span>Waiting response</span>
                        </div>
                    </div>
                    <div>
                        <div>
                            <span>Country:</span>
                            <span>{{user.country}}</span>
                        </div>
                        <div>
                            <span>City/Town:</span>
                            <span>{{user.city}}</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>`,
    props: ['user', 'filter'],
    data: function() {
        return {
            default_avatar: 'static/images/user-avatar.jpg',
            searchText: '',
            users_list: ''
        }
    },
    created() {
        this.$http.get('user/find/').then(response => {
            this.users_list = response.body;
        }, response => {
            console.log(response);
            window.location.href='#/login';
        });
    },
    methods: {
    }
};