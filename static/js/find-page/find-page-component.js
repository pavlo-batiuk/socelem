const FindPage = {
    template:
    `<div class="col-xs-12 base-max-width">
        <div class="col-xs-3 profile-container">
            <filter-search v-bind:user="user" v-bind:filter="filter"></filter-search>
        </div>
        <div class="col-xs-6">
            <div class="main-content-container">
                <main-search v-bind:user="user" v-bind:filter="filter"></main-search>
            </div>
        </div>
        <div class="col-xs-3 navigate-container">
            <navigation v-bind:user="user"></navigation>
        </div>
    </div>`,
    components: {
        'filter-search': FilterSearch,
        'navigation': Navigation,
        'main-search': MainSearch
    },
    data: function() {
        return {
            is_show: false,
            user: {},
            filter: {
                country: '',
                state: '',
                city: '',
                street: '',
                birthday: ''
            }
        }
    },
    created() {
    }
};
