const FilterSearch = {
    template:
    `<div class="inner-profile-container">
        <div class="m-b-15 m-l-10 main-text-color uppercase information-title">
            <span>Filter search:</span>
        </div>     
        <div class="f15">
            <div class="m-b-15">
                <input class="search-filter-input" type="text" placeholder="Country" v-model="filter.country">
            </div>
            <div class="m-b-15">
                <input class="search-filter-input" type="text" placeholder="State" v-model="filter.state">
            </div>
            <div class="m-b-15">
                <input class="search-filter-input" type="text" placeholder="City" v-model="filter.city">
            </div>
            <div class="m-b-15">
                <input class="search-filter-input" type="text" placeholder="Street" v-model="filter.street">
            </div>
            <div class="m-b-15">
                <input class="search-filter-input" type="text" placeholder="Birthday" v-model="filter.birthday">
            </div>
        </div>
        <div v-if="!show_add_sex">
            <a href="javascript:void(0)" v-on:click="showSexFilter()" class="f16 information-title">
                <span class="glyphicon glyphicon-plus"></span> 
                <span>Add sex filter</span>
            </a>
        </div>
        <div v-if="show_add_sex">
            <a href="javascript:void(0)" v-on:click="showSexFilter()" class="f16 information-title">
                <span class="glyphicon glyphicon-minus"></span> 
                <span>Remove sex filter</span>
                <hr>
            </a>  
            <div class="radio m-b-15">
                <input id="male" type="radio" value="true" v-model="is_male" v-on:change="changeSex(is_male)">
                <label class="radio-label f15" for="male">Male</label>
                <input id="female" type="radio" value="false" v-model="is_male" v-on:change="changeSex(is_male)">
                <label class="radio-label f15" for="female">Female</label>
            </div>
        </div>
    </div>`,
    props: ['user', 'filter'],
    data: function() {
        return {
            is_show: false,
            show_add_sex: false,
            is_male: true
        }
    },
    methods: {
        showSexFilter: function() {
            this.show_add_sex = !this.show_add_sex;
            this.show_add_sex? (this.filter['is_male'] = this.is_male): (delete this.filter['is_male']);
        },
        changeSex: function(is_male) {
            this.filter.is_male = is_male;
        }
    }
};
