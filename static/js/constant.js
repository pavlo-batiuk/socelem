var errorMessages = {
    emailRequire: 'Email is required',
    emailNotValid: 'Email must be valid',
    passwordTooShort: 'Password too short. Required 6 or more characters',
    passwordDontMatch: 'Password do not match'
};

var navigateItems = [
    { name:'Home page', link: '/'},
    { name:'Friends', link: '/friends'},
    { name:'Groups', link: '/groups'},
    { name:'Photos', link: '/photos'},
    { name:'Settings', link: '/settings'}
];