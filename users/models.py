from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from users.managers import UserManager
from django.db import models
import random
import uuid

SEARCH_TEXT = 'search_text'


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    first_name = models.CharField(max_length=80)
    last_name = models.CharField(max_length=80)
    email = models.EmailField(max_length=60, unique=True)
    username = models.CharField(max_length=255, unique=False)
    password = models.CharField(max_length=255)
    avatar_url = models.URLField(max_length=300, blank=True, unique=False)
    country = models.CharField(max_length=255, default='')
    state = models.CharField(max_length=255, default='')
    city = models.CharField(max_length=255, default='')
    street = models.CharField(max_length=255, default='')
    birthday = models.DateField(null=True)
    is_male = models.BooleanField(default=True)
    friends = models.ManyToManyField('self', blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    @staticmethod
    def get_user_by_id(user_id):
        return User.objects.get(id=user_id)

    def find_user_by_filters(self, parameters, user_id):
        filters_dict = self.get_filter_parameters(parameters)
        if not parameters.get(SEARCH_TEXT):
            found_users = User.objects.filter(**filters_dict).exclude(id=user_id)[:5]
        else:
            search_words_list = self.__get_search_word_list(parameters.get(SEARCH_TEXT))
            found_users = User.objects.filter(**filters_dict).filter(
                reduce(lambda x, y: x | y,
                [models.Q(**{'first_name__istartswith': word}) |
                 models.Q(**{'last_name__istartswith': word})
                for word in search_words_list])
            ).exclude(id=user_id)[:5]
        return found_users

    @staticmethod
    def __get_search_word_list(search_text):
        names = search_text.split(' ')
        search_words_list = [search_text]
        if len(names) > 1:
            search_words_list.extend(names)
        return search_words_list

    @staticmethod
    def get_filter_parameters(parameters):
        filters_dict = {}
        for key, value in parameters.iteritems():
            if key == SEARCH_TEXT or not value:
                continue
            if key == 'is_male':
                filters_dict[key] = User.convert_json_bool_to_python(value)
            else:
                filters_dict[key] = value
        return filters_dict

    @staticmethod
    def convert_json_bool_to_python(value):
        return ('true' in value)

    @staticmethod
    def get_random_users(user_id):
        users = list(User.objects.exclude(id=user_id))
        random.shuffle(users)
        return users[:5]


class FriendRequest(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    requested = models.ForeignKey(User, related_name='requested')
    assigned = models.ForeignKey(User, related_name='assigned')

    @staticmethod
    def create_request(assigned_user, request_user):
        return FriendRequest.objects.create(assigned=assigned_user, requested=request_user)