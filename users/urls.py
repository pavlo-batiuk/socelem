from django.conf.urls import url
from rest_framework. authtoken.views import obtain_auth_token
from users import views

urlpatterns = [
    url(r'^$', views.UserView.as_view(), name='user'),
    url(r'^profile/(?P<user_id>[a-zA-Z0-9\-]+)/$', views.ProfileView.as_view(), name='user-profile'),
    url(r'^find/$', views.FindUsersView.as_view(), name='find-users'),
    url(r'^search/', views.SearchUserView.as_view(), name='search-users')
]