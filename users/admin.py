from django.contrib import admin
from users.models import User, FriendRequest


class AccountsAdmin(admin.ModelAdmin):
    list_display = ('id', 'last_name', 'email')
admin.site.register(User, AccountsAdmin)
admin.site.register(FriendRequest)
