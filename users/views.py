from django.shortcuts import render, render_to_response
from rest_framework import views, generics, status
from rest_framework.authtoken.models import Token
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.authentication import TokenAuthentication
from rest_framework import permissions
from users.models import User, FriendRequest
from users.serializers import UserSerializer
from content.serializers import PostSerializer


class UserView(generics.RetrieveAPIView):
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def retrieve(self, request, *args, **kwargs):
        try:
            user_id = request.user.id
            if not user_id:
                return Response('error', status=status.HTTP_400_BAD_REQUEST)
            user = User.get_user_by_id(user_id)
            user_serializer = self.serializer_class(user, context={'request': request})
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        except (TypeError, KeyError, ObjectDoesNotExist, IntegrityError) as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)


class ProfileView(views.APIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get(self, request, **kwargs):
        try:
            user_id = kwargs['user_id']
            if not user_id:
                return Response('error', status=status.HTTP_400_BAD_REQUEST)
            user = User.objects.get(id=user_id)
            user_serializer = UserSerializer(user, context={'request': request})
            user_posts = user.owner.all()
            posts_serializer = PostSerializer(user_posts, many=True, context={'request': request})
            posts_serializer.context['user'] = request.user
            user_data = {'profile': user_serializer.data, 'posts': posts_serializer.data}
            return Response(user_data,status=status.HTTP_200_OK)
        except IntegrityError as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        except ObjectDoesNotExist as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        except (TypeError, KeyError) as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)


class FindUsersView(views.APIView):

    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get(self, request, **kwargs):
        try:
            user_id = request.user.id
            if not user_id:
                return Response('error', status=status.HTTP_400_BAD_REQUEST)
            random_users = User().get_random_users(user_id)
            user_serializer = UserSerializer(random_users, many=True, context={'request': request})
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        except (TypeError, KeyError, ObjectDoesNotExist, IntegrityError) as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, **kwargs):
        try:
            user_id = request.data.get('userId')
            if not user_id:
                return Response('error', status=status.HTTP_400_BAD_REQUEST)
            user = User.get_user_by_id(user_id)
            FriendRequest.create_request(user, request.user)
            user_serializer = UserSerializer(user, context={'request': request})
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        except (TypeError, KeyError, ObjectDoesNotExist, IntegrityError) as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)


class SearchUserView(views.APIView):

    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get(self, request, *args, **kwargs):
        try:
            user_id = request.user.id
            parameters = request.GET
            if not parameters:
                return Response('Please enter filters to search', status=status.HTTP_400_BAD_REQUEST)
            users = User().find_user_by_filters(parameters, user_id)
            user_serializer = UserSerializer(users, many=True, context={'request': request})
            return Response(user_serializer.data, status=status.HTTP_200_OK)
        except (TypeError, KeyError, ObjectDoesNotExist, IntegrityError) as error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)