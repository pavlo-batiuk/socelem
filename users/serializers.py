from users.models import User, FriendRequest
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    state = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    street = serializers.CharField(required=False)
    birthday = serializers.DateField(required=False)
    friend_request = serializers.SerializerMethodField(source='get_friend_request', required=False)
    friend_assign_count = serializers.SerializerMethodField(source='get_friend_assign_count', required=False)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'password', 'avatar_url',
                  'country', 'state', 'city', 'street', 'birthday', 'is_male',
                  'friend_request', 'friend_assign_count')

    def get_friend_request(self, obj):
        user = self.context.get('user')
        if not user:
            request = self.context.get('request', None)
            user = request.user if request else self.instance
        return FriendRequest.objects.filter(requested=user, assigned=obj).exists()

    def get_friend_assign_count(self, obj):
        return FriendRequest.objects.filter(assigned=obj).count()
